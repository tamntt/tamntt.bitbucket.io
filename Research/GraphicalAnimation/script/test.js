function draw(){

    var data  = loadData();
    allData = data;
    initTime();
    svg = d3.select("#svgPicture").select("svg");
    elementDisplayState = d3.select("#diplayStateText");
    MakeTransition(data, speed);
    
}

function stopSVG(){
    svg.selectAll("text").transition().duration(0).delay(0);
    
    d3.select("#diplayStateText").transition().duration(0).delay(0);
    
    clearTimeout(running);
    
    var index = d3.select("#diplayStateText").attr("stepIndex");
    
    if(!checkUndefined(index)){
        if(Number(index) > parseInt(index)) stepIndex = parseInt(index) + 1;
        else stepIndex = parseInt(index);
        
        if(stepIndex >= allData.length){
            stepLoopIndex = stepIndex - allData.length;
        }
    }
    
   
}



function backStep(){
    var loopLength = loopStates.length;
    
    if(stepIndex == 0 || (checkUndefined(allData.length)))
    {
        allData  = loadData();
        stepIndex = lengthData - 1;
        if(loopStates.length > 0 ){
            stepIndex = stepIndex + loopLength;
            stepLoopIndex = loopLength - 1;
            
        }
        svg = d3.select("#svgPicture").select("svg");
        elementDisplayState = d3.select("#diplayStateText");
        
    }
    else{
        stepIndex = stepIndex - 1;
        if(stepIndex > lengthData) stepLoopIndex = stepIndex - lengthData;
    }
    
    drawStep();
    stepIndex = stepIndex - 1;
    
    
}
    

function drawStep(){
    
    if(stepIndex == 0 || (checkUndefined(allData.length)))
    {
        allData  = loadData();
        svg = d3.select("#svgPicture").select("svg");
        elementDisplayState = d3.select("#diplayStateText");
        lengthData = allData.length ;
    }
    initTime();
    var state; ;
    var next_state;
    
    if(stepIndex < lengthData){
        if(stepIndex == 0){
            state= allData[stepIndex];
            next_state = state;
            allKeys = Object.keys(state);
            var index = 0;
            StateGraph(state, next_state,index);
        }
        else{
            state= allData[stepIndex-1];
            next_state = allData[stepIndex];
            StateGraph(state, next_state,stepIndex);
        }
        stepIndex = stepIndex + 1;
    }
    else{
        if(lengthLoopStates == 0){
            stepIndex = 0;
            clearTimeout(running);
           // stopSVG();
        }
        else{
            if(stepLoopIndex == 0){
                if(loop) state = loopStates[lengthLoopStates - 1];
                else state= allData[stepIndex - 1 ];
                next_state = loopStates[0];
                StateGraph(state, next_state,stepIndex);
                stepLoopIndex = stepLoopIndex + 1;
                stepIndex = stepIndex + 1;
            }
            else{
                if(stepLoopIndex < loopStates.length){
                    state= loopStates[stepLoopIndex-1];
                    next_state = loopStates[stepLoopIndex];
                    
                    StateGraph(state, next_state,stepIndex);
                    
                    stepLoopIndex = stepLoopIndex + 1;
                    stepIndex = stepIndex + 1;
                }
                else{
                    stepIndex = stepIndex - stepLoopIndex ;
                    stepLoopIndex = 0;
                    loop = true;
                    
                }
            }
        }
    }
}



function initTime()
{
    speed =$('#slider').slider('value') ;
    wait = speed/2;
    time = 0;
    
    //init groups: disable
    
}

function MakeTransition(data, speed)
{
    
    var length = data.length ;
//    for(var i = stepIndex; i < length ; i++){
//        var state = data[i];
//        // get all keys from state input.
//        var next_state;
//        if(i < length){
//            if(i == 0){
//                state = data[i];
//                next_state = state;
//                allKeys = Object.keys(state);
//                StateGraph(state, next_state,0);
//            }
//            else{
//                state= allData[i-1];
//                next_state = allData[i];
//                StateGraph(state, next_state,i);
//            }
//        }
//    }
 
    running =   setInterval(function(){drawStep(); }, time + wait + speed);
  
    
}


function StateGraph(state, next_state,index){
    
    $(".groups").removeClass("show");
    $(".groups").addClass("hidden");
    //d3.selectAll(".groups").attr("class","groups hidden");
    
    var numKeys = allKeys.length ;
    var k, s1 , s2, element;
    
    var ruleKeys = Object.keys(allRules);
    var lengthOfRules = ruleKeys.length;
    
    var changeSate = {};
    var displayState = "" ;
    var initSate = "" ;
    for (var i= 0; i< numKeys; i++){
        key = allKeys[i];
        s1 = state[key].trim();
        s2 = next_state[key].trim();
        displayState = displayState + " (" + key + ": " + s2  + " )";
        
        element =  svg.selectAll("[id='" + svgIDs[i] + "']" );
        var tmpValue = texDisplays[key];
        if(checkUndefined(element) == false && element[0].length > 0 )
        {
            if(index == 0)
            {
                initSate = initSate + "#" + key + ": " + s1 + " " ;
                if(!checkUndefined(tmpValue)){
                    var option = tmpValue[0];
                    if(option == "VER" || option == "VER-REV"){
                       // NormalizeTextDisplay(key,s1, "black", speed, time);
                        setTimeout(NormalizeTextDisplay(key,s1, "black", speed, time), time);
                        time = time + wait;

                    }
                    if(option == "REV" || option == "NONE"){
                         element.transition().text(NormalizeText(key,s1)).attr("fill", "black").duration(speed).delay(time);
                    }
                }
                else{
                     element.transition().text(s1).attr("fill", "black").duration(speed).delay(time);
                }
                
            }
            var color ;
            if(s1 != s2) color = "red";
            else color = "black";
            
            if(!checkUndefined(tmpValue)){
                var option = tmpValue[0];
                if(option == "VER" || option == "VER-REV"){
                    setTimeout(NormalizeTextDisplay(key,s2, color, speed, time + wait), time + wait);
                     time = time + wait;
                }
                if(option == "REV" || option == "NONE"){
                    element.transition().text(NormalizeText(key,s2)).attr("fill", color).duration(speed).delay(time + wait);
                }
            }
            else{
                element.transition().text(s2).attr("fill", color).duration(speed).delay(time + wait);
            }
        }
        else{ // get groups elements
            element =  svg.selectAll("[id='" + svgIDs[i] + "_" + s2.replace("'", "&#39;") + "']" );
            if(!checkUndefined(element) && element[0].length > 0){
                element.attr("class", "groups show");
            }
        }
        var classElement = document.getElementsByClassName(s2);
        
        if(classElement){
           // var classElementColor = element.attr("fill");
            
            for(var k = 0; k< classElement.length; k++){
                classElement[k].classList.remove("hidden");
                classElement[k].classList.add("show");
              //  classElement[k].setAttribute("fill", classElementColor);

            }
        }
        
        
      // display items which have mapped values 
        var element =  svg.selectAll("[id='" + s2.replace("'", "&#39;") + "']" );
        if(!checkUndefined(element) && element[0].length > 0){
                    element.attr("class", "groups show")
        }
        
        
        //s2_class.classList.add("show");
        
    }
//    if(index == 0)
//    {
//        elementDisplayState.transition().text("State 0 :" + displayState).attr("fill", "black").attr("stepIndex", initSate.toString()).duration(speed).delay(time);
//        time = speed;
//    }
    
    $("#diplayStateText").html("State " + (index) + " :" + displayState);
        
//    elementDisplayState.transition().text("State " + (index) + " :"
//                    + displayState).attr("fill", "black").attr("stepIndex", index.toString()).duration(speed).delay(time);
//

}




	
			
			
   