mod! PNAT {
  [Nat]
  op 0 : -> Nat 
  op s : Nat -> Nat 
  op _=_ : Nat Nat -> Bool {comm}
  vars X Y : Nat 
  eq (X = X) = true .
  eq (0 = 0) = true .
  eq (0 = s(X)) = false .
  eq (s(X) = s(Y)) = (X = Y) .
}

mod* PACKET {
  pr(PNAT)
  [Packet]
  op pac : Nat -> Packet
  op _=_ : Packet Packet -> Bool {comm}
  var P : Packet
  vars X Y : Nat
  eq (P = P) = true .
  eq (pac(X) = pac(Y)) = (X = Y) .
}

mod! EQBOOL {
  op _=_ : Bool Bool -> Bool {comm}
  vars B B1 B2 : Bool
  eq (B = B) = true .
  eq (true = false) = false .
-- Lemmas
  op eqbool-lemma1 : Bool -> Bool
  eq eqbool-lemma1(B) = not((not B) = B) .
  op eqbool-lemma2 : Bool Bool Bool -> Bool
  eq eqbool-lemma2(B,B1,B2) = (B = B1) or (B = B2) or (B1 = B2) .
}

mod* EQTRIV {
  [Elt]
  op _=_ : Elt Elt -> Bool {comm}
}
 
mod! PAIR(M :: EQTRIV,N :: EQTRIV) {
  [Pair]
  op <_,_> : Elt.M Elt.N -> Pair
  op fst : Pair -> Elt.M
  op snd : Pair -> Elt.N
  op _=_ : Pair Pair -> Bool {comm}
  vars P P' : Pair
  vars X1 X2 : Elt.M
  vars Y1 Y2 : Elt.N
  eq fst(< X1,Y1 >) = X1 .
  eq snd(< X1,Y1 >) = Y1 .
  eq (P = P) = true .
  eq (< X1,Y1 > = < X2,Y2 >) = (X1 = X2) and (Y1 = Y2) .
-- Lemmas
  op pair-lemma1 : Pair Pair -> Bool
  eq pair-lemma1(P,P') = (P = P') iff (fst(P) = fst(P') and snd(P) = snd(P')) .
}

mod! QUEUE (M :: EQTRIV) {
  [Queue]
  op empty : -> Queue
  op _,_ : Elt.M Queue -> Queue
  op put : Queue Elt.M -> Queue
  op get : Queue -> Queue
  op top : Queue -> Elt.M
  op _\in_ : Elt.M Queue -> Bool
  op _@_ : Queue Queue -> Queue
  op del : Queue -> Queue
  op bot : Queue -> Elt.M
  op _=_ : Queue Queue -> Bool {comm}
  vars X X' X'' X''' :  Elt.M
  vars Q Q' Q'' : Queue
  eq put(empty,X) = X,empty .
  eq put((X',Q),X) = X',put(Q,X) .
  eq get((X,Q)) = Q .
  eq top((X,Q)) = X .
  eq X \in empty = false .
  eq X \in (X',Q) = (X = X') or X \in Q .
  eq empty @ Q = Q .
  eq (X',Q') @ Q = X',(Q' @ Q) .
  eq del(empty) = empty .
  eq del(X,empty) = empty .
  eq del(X,X',Q) = X,del(X',Q) .
  eq bot(X,empty) = X .
  eq bot(X,X',Q) = bot(X',Q) .
  eq (Q = Q) = true .
  eq (empty = X',Q') = false .
  eq (X,Q = X',Q') = (X = X') and (Q = Q') .
  --
-- Lemmas
  op queue-lemma1 : Queue Queue Elt.M Elt.M Elt.M -> Bool
  eq queue-lemma1(Q,Q',X,X',X'') 
     = (put(Q,X) = Q' @ (X',X'',empty)) iff (Q = Q' @ (X',empty)) and (X = X'') .
  op queue-lemma2 : Queue Queue Queue Elt.M Elt.M Elt.M Elt.M -> Bool
  eq queue-lemma2(Q,Q',Q'',X,X',X'',X''') 
     = (put(Q,X) = Q' @ (X',X'',X''',Q'')) 
       iff (Q = del(Q' @ (X',X'',X''',Q''))) and (X = bot(X''',Q'')) .
  op queue-lemma3 : Queue Elt.M Elt.M -> Bool
  eq queue-lemma3(Q,X,X') = X \in X',Q and not(X \in del(X',Q)) 
                            implies (X = bot(X',Q)) .
  op queue-lemma4 : Queue Queue Elt.M -> Bool
  eq queue-lemma4(Q,Q',X) = X \in (Q @ (X,Q')) .
  op queue-lemma5 : Queue Elt.M Elt.M -> Bool
  eq queue-lemma5(Q,X,X') = X \in put(Q,X') iff (X = X' or X \in Q) .
  op queue-lemma6 : Queue Queue -> Bool 
  eq queue-lemma6(Q,Q') = ((Q @ Q') = empty) iff (Q = empty) and (Q' = empty) .
  op queue-lemma7 : Queue Queue Elt.M -> Bool
  eq queue-lemma7(Q,Q',X) = del(Q @ (X,Q')) = Q @ del(X,Q') .
}

mod! LIST(M :: EQTRIV) {
  [List]
  op nil : -> List
  op __ : Elt.M List -> List
  op hd : List -> Elt.M
  op tl : List -> List 
  op _=_ : List List -> Bool {comm}
  vars L L1 L2 : List
  vars X Y : Elt.M
  eq hd(X L) = X .
  eq tl(X L) = L .
  eq (L = L) = true .
  eq (nil = nil) = true .
  eq (nil = X L) = false .
  eq (X L1 = Y L2) = (X = Y and L1 = L2) .
}

view EQTRIV2PACKET from EQTRIV to PACKET {
  sort Elt -> Packet,
  op _=_ -> _=_
}

mod! PACKET-LIST {
  pr(LIST(M <= EQTRIV2PACKET))
  op mk : Nat -> List 
  var X : Nat
  eq mk(0) = pac(0) nil .
  eq mk(s(X)) = pac(s(X)) mk(X) .
}

view EQTRIV2EQBOOL from EQTRIV to EQBOOL {
  sort Elt -> Bool,
  op _=_ -> _=_
}

mod! BOOL-PACKET-PAIR {
  pr(PAIR(M <= EQTRIV2EQBOOL,N <= EQTRIV2PACKET)*{sort Pair -> BPPair})
}

view EQTRIV2BOOL-PACKET-PAIR from EQTRIV to BOOL-PACKET-PAIR {
  sort Elt -> BPPair,
  op _=_ -> _=_
}

mod! BOOL-QUEUE {
  pr(QUEUE(M <= EQTRIV2EQBOOL)*{sort Queue -> BFifo})
}

mod! BOOL-PACKET-PAIR-QUEUE {
  pr(QUEUE(M <= EQTRIV2BOOL-PACKET-PAIR)*{sort Queue -> PFifo})
}

mod* ABP {
  pr(PNAT + PACKET-LIST)
  pr(BOOL-QUEUE)
  pr(BOOL-PACKET-PAIR-QUEUE)
  *[Sys]*
-- initial states
  op init : -> Sys
-- observations
  bop fifo1 : Sys -> PFifo -- Sender-to-Receiver channel
  bop fifo2 : Sys -> BFifo -- Receiver-to-Sender channel
  bop bit1  : Sys -> Bool  -- Sender's bit
  bop bit2  : Sys -> Bool  -- Receiver's bit
  bop next  : Sys -> Nat   -- the ordinal of the packet sent next by Sender
  bop list  : Sys -> List  -- the packets received by Receiver
-- actions
  bop send1 : Sys -> Sys -- Sender's sending pairs of bits&packets
  bop rec1  : Sys -> Sys -- Sender's receiving bits
  bop send2 : Sys -> Sys -- Receiver's sending bits
  bop rec2  : Sys -> Sys -- Receiver's receiving pairs of bits&packets
  bop drop1 : Sys -> Sys -- dropping the 1st of fifo1
  bop dup1  : Sys -> Sys -- duplicating the 1st of fifo1
  bop drop2 : Sys -> Sys -- dropping the 1st of fifo2
  bop dup2  : Sys -> Sys -- duplicating the 1st of fifo2
-- CafeOBJ variables
  var S : Sys
-- for initial state
  eq fifo1(init) = empty .
  eq fifo2(init) = empty .
  eq bit1(init)  = false .
  eq bit2(init)  = false .
  eq next(init)  = 0 .
  eq list(init)  = nil .
-- send1
  eq fifo1(send1(S)) = put(fifo1(S),< bit1(S),pac(next(S)) >) .
  eq fifo2(send1(S)) = fifo2(S) .
  eq bit1(send1(S))  = bit1(S) .
  eq bit2(send1(S))  = bit2(S) .
  eq next(send1(S))  = next(S) .
  eq list(send1(S))  = list(S) .
-- rec1
  op c-rec1 : Sys -> Bool
  eq c-rec1(S) = not(fifo2(S) = empty) .
  --
  eq  fifo1(rec1(S)) = fifo1(S) .
  ceq fifo2(rec1(S)) = get(fifo2(S)) if c-rec1(S) .
  ceq bit1(rec1(S))
      = (if bit1(S) = top(fifo2(S)) then bit1(S) else top(fifo2(S)) fi)
      if c-rec1(S) .
  eq  bit2(rec1(S))  = bit2(S) .
  ceq next(rec1(S))  
      = (if bit1(S) = top(fifo2(S)) then next(S) else s(next(S)) fi)
      if c-rec1(S) .
  eq list(rec1(S))  = list(S) .
  bceq rec1(S)      = S if not c-rec1(S) .
-- send2
  eq fifo1(send2(S)) = fifo1(S) .
  eq fifo2(send2(S)) = put(fifo2(S),bit2(S)) .
  eq bit1(send2(S))  = bit1(S) .
  eq bit2(send2(S))  = bit2(S) .
  eq next(send2(S))  = next(S) .
  eq list(send2(S))  = list(S) .
-- rec2
  op c-rec2 : Sys -> Bool
  eq c-rec2(S) = not(fifo1(S) = empty) .
  --
  ceq fifo1(rec2(S)) = get(fifo1(S)) if c-rec2(S) .
  eq  fifo2(rec2(S)) = fifo2(S) .
  eq  bit1(rec2(S))  = bit1(S) .
  ceq bit2(rec2(S))  
      = (if bit2(S) = fst(top(fifo1(S))) 
         then not fst(top(fifo1(S))) else bit2(S) fi)
      if c-rec2(S) .
  eq  next(rec2(S))  = next(S) .
  ceq list(rec2(S))  
      = (if bit2(S) = fst(top(fifo1(S))) 
         then (snd(top(fifo1(S))) list(S)) else list(S) fi) 
      if c-rec2(S) .
  bceq rec2(S)      = S if not c-rec2(S) .
-- drop1
  op c-drop1 : Sys -> Bool
  eq c-drop1(S) = not(fifo1(S) = empty) .
  --
  ceq fifo1(drop1(S)) = get(fifo1(S)) if c-drop1(S) .
  eq  fifo2(drop1(S)) = fifo2(S) .
  eq  bit1(drop1(S))  = bit1(S) .
  eq  bit2(drop1(S))  = bit2(S) .
  eq  next(drop1(S))  = next(S) .
  eq  list(drop1(S))  = list(S) .
  bceq drop1(S)       = S if not c-drop1(S) .
-- dup1
  op c-dup1 : Sys -> Bool
  eq c-dup1(S) = not(fifo1(S) = empty) .
  --
  ceq fifo1(dup1(S)) = top(fifo1(S)),fifo1(S) if c-dup1(S) .
  eq  fifo2(dup1(S)) = fifo2(S) .
  eq  bit1(dup1(S))  = bit1(S) .
  eq  bit2(dup1(S))  = bit2(S) .
  eq  next(dup1(S))  = next(S) .
  eq  list(dup1(S))  = list(S) .
  bceq dup1(S)       = S if not c-dup1(S) .
-- drop2
  op c-drop2 : Sys -> Bool
  eq c-drop2(S) = not(fifo2(S) = empty) .
  --
  eq  fifo1(drop2(S)) = fifo1(S) .
  ceq fifo2(drop2(S)) = get(fifo2(S)) if c-drop2(S) .
  eq  bit1(drop2(S))  = bit1(S) .
  eq  bit2(drop2(S))  = bit2(S) .
  eq  next(drop2(S))  = next(S) .
  eq  list(drop2(S))  = list(S) .
  bceq drop2(S)       = S if not c-drop2(S) .
-- dup2
  op c-dup2 : Sys -> Bool
  eq c-dup2(S) = not(fifo2(S) = empty) .
  --
  eq  fifo1(dup2(S)) = fifo1(S) .
  ceq fifo2(dup2(S)) = top(fifo2(S)),fifo2(S) if c-dup2(S) .
  eq  bit1(dup2(S))  = bit1(S) .
  eq  bit2(dup2(S))  = bit2(S) .
  eq  next(dup2(S))  = next(S) .
  eq  list(dup2(S))  = list(S) .
  bceq dup2(S)       = S if not c-dup2(S) .
}

