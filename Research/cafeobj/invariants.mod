mod INV {
  pr(ABP)
-- arbitrary values
  op s : -> Sys 
  ops bit bit1 bit2 bit3 : -> Bool
  ops pair pair1 pair2 pair3 : -> BPPair
  ops bfifo bfifo1 bfifo2 : -> BFifo 
  ops pfifo pfifo1 pfifo2 : -> PFifo 
-- names of invariants
  op inv1 : Sys -> Bool
  op inv2 : Sys -> Bool
  op inv3 : Sys -> Bool
  op inv4 : Sys Bool -> Bool
  op inv5 : Sys Bool -> Bool
  op inv6 : Sys BPPair -> Bool
  op inv7 : Sys BPPair -> Bool
  op inv8 : Sys Bool Bool Bool BFifo BFifo -> Bool
  op inv9 : Sys BPPair BPPair BPPair PFifo PFifo -> Bool
  op inv10 : Sys Bool -> Bool
  op inv11 : Sys BPPair -> Bool
-- CafeOBJ variables
  var S : Sys
  vars BIT BIT1 BIT2 BIT3 : Bool
  vars PAIR PAIR1 PAIR2 PAIR3 : BPPair
  vars BFIFO BFIFO1 BFIFO2 : BFifo
  vars PFIFO PFIFO1 PFIFO2 : PFifo
-- invariants
  eq inv1(S) 
     = (bit1(S) = bit2(S) implies mk(next(S)) = (pac(next(S)) list(S))) and
       (not(bit1(S) = bit2(S)) implies mk(next(S)) = list(S)) .
  eq inv2(S)
      = not(fifo2(S) = empty)
        implies ((bit1(S) = top(fifo2(S))) or (bit2(S) = top(fifo2(S)))) .
  eq inv3(S) 
     = (not(fifo1(S) = empty) and bit2(S) = fst(top(fifo1(S))))
       implies 
       (bit1(S) = fst(top(fifo1(S))) and pac(next(S)) = snd(top(fifo1(S)))) .
  eq inv4(S,BIT)
     = (not(fifo2(S) = empty) and not(bit1(S) = top(fifo2(S))) and BIT \in fifo2(S))
       implies (top(fifo2(S)) = BIT) .
  eq inv5(S,BIT)
     = (not(fifo2(S) = empty) and BIT \in fifo2(S) and not(bit1(S) = BIT))
       implies (bit2(S) = BIT) .
  eq inv6(S,PAIR)
     = (not(fifo1(S) = empty) and bit2(S) = fst(top(fifo1(S))) and PAIR \in fifo1(S))
       implies (top(fifo1(S)) = PAIR) .
  eq inv7(S,PAIR)
     = (not(fifo1(S) = empty) and PAIR \in fifo1(S) and bit2(S) = fst(PAIR))
       implies (bit1(S) = fst(PAIR) and pac(next(S)) = snd(PAIR)) .
  eq inv8(S,BIT1,BIT2,BIT3,BFIFO1,BFIFO2)
     = ((fifo2(S) = BFIFO1 @ (BIT1,BIT2,BFIFO2) and not(BIT1 = BIT2))
        implies ((BIT3 \in BFIFO2 implies BIT2 = BIT3)  and BIT2 = bit2(S))) .
  eq inv9(S,PAIR1,PAIR2,PAIR3,PFIFO1,PFIFO2)
     = ((fifo1(S) = PFIFO1 @ (PAIR1,PAIR2,PFIFO2) and not(PAIR1 = PAIR2))
        implies ((PAIR3 \in PFIFO2 implies PAIR2 = PAIR3) 
                 and PAIR2 = < bit1(S),pac(next(S)) >)) .
  eq inv10(S,BIT)
     = ((bit1(S) = bit2(S)) implies (BIT \in fifo2(S) implies BIT = bit2(S))) .
  eq inv11(S,PAIR)
     = (not(bit1(S) = bit2(S)) 
        implies (PAIR \in fifo1(S) implies PAIR = < bit1(S),pac(next(S)) >)) .
}

mod ISTEP {
  pr(INV)
-- arbitrary objects
  op s' : -> Sys -- an arbitrary successor state of s
-- names of formulas to prove
  op istep1 : -> Bool
  op istep2 : -> Bool
  op istep3 : -> Bool
  op istep4 : -> Bool
  op istep5 : -> Bool
  op istep6 : -> Bool
  op istep7 : -> Bool
  op istep8 : -> Bool
  op istep9 : -> Bool
  op istep10 : -> Bool
  op istep11 : -> Bool
-- formulas to prove
  eq istep1 = inv1(s) implies inv1(s') .
  eq istep2 = inv2(s) implies inv2(s') .
  eq istep3 = inv3(s) implies inv3(s') .
  eq istep4 = inv4(s,bit) implies inv4(s',bit) .
  eq istep5 = inv5(s,bit) implies inv5(s',bit) .
  eq istep6 = inv6(s,pair) implies inv6(s',pair) .
  eq istep7 = inv7(s,pair) implies inv7(s',pair) .
  eq istep8 = inv8(s,bit1,bit2,bit3,bfifo1,bfifo2) 
               implies inv8(s',bit1,bit2,bit3,bfifo1,bfifo2) .
  eq istep9 = inv9(s,pair1,pair2,pair3,pfifo1,pfifo2) 
               implies inv9(s',pair1,pair2,pair3,pfifo1,pfifo2) .
  eq istep10 = inv10(s,bit) implies inv10(s',bit) .
  eq istep11 = inv11(s,pair) implies inv11(s',pair) .
}
